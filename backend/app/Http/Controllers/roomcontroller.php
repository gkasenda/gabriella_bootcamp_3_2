<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class roomcontroller extends Controller
{
    function addrent (Request $request){
        DB::beginTransaction();
        try{

 
            $room_id = $request -> input('room_id');
            $customer_id = $request -> input('customer_id');
            $payment_status = $request -> input('payment_status');
            $check_in = $request -> input ('check_in');


            $rent = new rent;
            $rent -> room_id = $room_id;
            $rent -> customer_id = $customer_id;
            $rent -> payment_status = $payment_status;
            $rent -> check_in = $check_in;
            $rent -> save();

            DB::commit();
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message" => $e->getMessage()], 500);
            
        }
}
