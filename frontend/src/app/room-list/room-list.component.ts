import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css']
})
export class RoomListComponent implements OnInit {

  @Input('room') roomList: Object[];

  constructor() { }

  ngOnInit() {
  }

}
