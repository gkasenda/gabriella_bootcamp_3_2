import { HotelRoomPage } from './app.po';

describe('hotel-room App', () => {
  let page: HotelRoomPage;

  beforeEach(() => {
    page = new HotelRoomPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
